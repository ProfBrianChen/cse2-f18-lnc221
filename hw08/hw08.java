//Lyle Chamberlain
//hw08

//import scanner and create main method
   import java.util.Scanner;
   import java.util.Random;
   import java.util.Arrays;
   
public class hw08{
  public static void main(String[]args){
 //create random string
    Random myRand = new Random();

Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 

String[] temp = new String[52];
 
int numCards = 0; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();

shuffle(cards);

//create scanner instance
Scanner myScan = new Scanner(System.in);

//printArray(cards); 
while(again == 1){ 
  
      System.out.print("how many cards would you like in the hand? ");
    numCards = myScan.nextInt();
  String[] myHand =  new String[numCards];
  if (index < numCards - 1){
    shuffle(cards);
    index = 51;
  }
    
     index = index - numCards;
    
     System.out.print("Your hand is: ");
myHand = getHand(cards,index,numCards); 
   //printArray(hand);
 

   System.out.println(Arrays.toString(myHand));
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } //end of main method
  
  
  
  
  //start of gethand method
  public static String[] getHand(String[]cards, int index,int numCards){

    String [] myHand =  new String[numCards];
    

    
    for(int i = 0; i < numCards;i++){
      myHand[numCards - i - 1] = cards[index+ numCards - i];
    }
    

 
return myHand;
  }
  
  
  
  
  
  //start of shuffle method
  public static String[] shuffle(String []cards){
    
    String [] temp = new String[52];
    Random myRand = new Random();
    for(int i = 0; i <52; i++){
      int j = myRand.nextInt(52);
      temp[j] = cards[i];
      cards[i] = cards[j];
   
      cards[j] = temp[j];
    }
      
      
      
    
    System.out.println("The shuffled cards are: ");
    System.out.println(Arrays.toString(cards));
    return cards;
    
  }   //end of shuffle method
  
  
}//end of class

