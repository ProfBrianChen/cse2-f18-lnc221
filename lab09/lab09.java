//lyle chamberlain
//lab09

//just import everything
import java.util.Arrays;
//import java.util.Scanner;
//import java.util.Random;
  
  public class lab09{
  //main method
  public static void main(String [] args){
    //create an int Array
    int [] intArray = {1,2,3,4,5};
    //create a copy
   int []copyArray = new int[intArray.length];
   //call a copy method for a copy array
    copyArray =  copy(intArray);
    inverter2(intArray);
    
    
    int [] array0 = {1,2,3,4,5,6,7,8,9};
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    
//pass array 0 through nverter and print it (6)
    inverter(array0);
    print(array0);
    
    System.out.println(" ");
    
    //Step 7
    //inverter2(array1);
    print(inverter2(array1));
   
    
    System.out.println(" " );
    
    int [] array3 = inverter2(array2);
    print(array3);
    
    System.out.println(" ");
    
    
    System.out.println("Arrays 0-3 printed out");
    System.out.println(Arrays.toString(array0));
    System.out.println(Arrays.toString(array1));
    System.out.println(Arrays.toString(array2));
     System.out.println(Arrays.toString(array3));
    

 

    
  }//end of main method
  
  
  //copy method
  public static int[] copy(int [] intArray){
    int[]copyArray = new  int[intArray.length];
    
    //copy intArray into your new one
    for(int i = 0; i < intArray.length;i++){
      copyArray[i] = intArray[i];
      
                       }
    //    System.out.print(Arrays.toString(copyArray));
    return copyArray;
         }//end of copy method
  
  //create inverter Class
  public static void inverter(int[] intArray){
     int[]temp = new int[intArray.length];
    
    for(int i = 0; i < intArray.length;i++){
      temp[i] = intArray[intArray.length - i - 1];
    }
    //now modify original
    for(int i = 0; i < intArray.length; i++){
      intArray[i] = temp[i];
    }//end of for loop
  
    return;
    
  }//end of inverter method
  
  
  //inverter2 method
  public static int[]inverter2(int [] copyArray){
    
    //get a copy of copy array
    int[] copy2 = copy(copyArray);
    //create a new array to accept the values
    int[]temp2 = new int[copy2.length];
    
    for(int i = 0; i < copy2.length;i++){
      temp2[i] = copy2[copy2.length - i - 1];
    }
    //now modify original
    for(int i = 0; i < copy2.length; i++){
      copy2[i] = temp2[i];
    }//end of for loop
  
    return copy2;
  }//end of inverter2 method
  
  
  //print method
  public static void print(int[]test){
   //print out array
    for(int i = 0; i < test.length; i++){
      System.out.print(" " + test[i] + " ");
    }//end of for loop
    
  }//end of print method
  
  
  
  
}//end of class