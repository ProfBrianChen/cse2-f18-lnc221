//hw09

import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
//import java.util.Arrays;
public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  
  //random method
  public static int[] randomInput(){
    //create random number generator
    Random myRand = new Random();
    int [] randomArray = new int[10];
    
    for(int i = 0; i < 10; i++){
      randomArray[i] = myRand.nextInt(10);
    }
 
    return randomArray;
  }//end of random input class
  
  //delete method
  public static int [] delete(int[] list, int pos){

   int myCount = list.length;
   System.out.println("List length is  " + myCount  );
   
    for(int i = 0; i < myCount; i++){

      if (i != 9){
      if (i >= pos){

      
        list[i] = list [i + 1];
        }
      }
      else{
       list[i] = 22;
      
      }
        
      
    }//end of for loop
    
    //create new array
    int heyo = list.length - 1;
    int[] newArray = new int[heyo];
    for(int i = 0; i < 9; i++){
      newArray[i] = list[i];
    }
         return newArray; 
  }//end of delete method
  
 
  //remove method
  public static int [] remove(int [] list,int target){
    System.out.print(Arrays.toString(list));
    int counter = 0;
    for (int i = 0; i < list.length - 1;i++){
     // if(target == list[i]){
      //  counter++;
     // }
  
      
      while(list[i + counter] == target){
        counter++;
      }
          //make sure we don't try to access huge elements
      if ((i + counter) > 8){
        break;
      }
      
      {
        list[i] = list[i + counter];
     }//end of else statement
    }//end of for loop
    
    int help = list.length - counter - 1;
    //create my own array and see if it works
    int[] myList = new int[help];
    
    for(int i = 0; i < myList.length - 0;i++){
     myList[i] = list[i];
    }
    
    return myList;
  }//end of remove method
   
  
}//end of class