//Lyle Chamberlain
//hw09

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
  
  
public class CSE2Linear{
  //main method
  public static void main(String[]args){
    
    //ask user for input
    System.out.println("Input 15 ascending ints: ");
    
    //create an array and Scanner
    int[]myArray = new int[15];
    Scanner myScan = new Scanner(System.in);
    //create a random generator
    Random myRand = new Random();
    
    
    boolean hi = false;
    //create a test variable to carry over the varaiable
    int j = 0;
    //create a variable to store user value
    int userVal = 0;
    
    
    
    for(int i = 0;i<15;i++){
      //create holder variable
      
      do{
        hi = myScan.hasNextInt();
        //print error message
        
        if(hi == false){
          System.out.print("Error, give an int ");
          //reset scanner
          myScan.next();
        
        }//end of if loop
        else{
          userVal = myScan.nextInt();
        }
      }//end of do loop
       while(hi == false);
       
       
       //test for out of range
       do{
       if((userVal < 0) || (userVal > 100)){
         System.out.println("Error, give a number within 0-100 ");
         userVal = myScan.nextInt();
         j = 5;
       }//end of if statement
       else{
         j = 10;
       }//end of else statement
       }//end of do statement
       while(j == 5);
      
      
   
      myArray[i] = userVal;
      
      //check if the numbers are greater than the previous
      do{
      //only enter the greater than test when its not the first element
        if((i != 0) && (myArray[i-1] > myArray[i])){
          System.out.println("Error, input an integer larger than the last recorded integer ");
          myArray[i] = myScan.nextInt();
        }
        else if((myArray[i] > 100) || (myArray[i] < 0)){
          System.out.println("Error, input a number within the range of 0-100 ");
          myArray[i] = myScan.nextInt();
        }
   
        else{
          j = 5;
        }//end of else statement
      }//end of do loop
      while(j == 10);
      
    }//end of 15 array input
    binary(myArray);
     Scramble(myArray);
    System.out.println(Arrays.toString(myArray));
    
    //call linear serch method
    linear(myArray);
  
    
  }//end of main method
  
  //create a scramble method
  public static int[] Scramble(int [] myArray){
    
    Random myRand = new Random();
    
    for(int i = 0; i < 15; i++){
      int y = myRand.nextInt(15);
      int temp = myArray[i];
      myArray[i] = myArray[y];
      myArray[y] = temp;
    }//end of for loop
     
    System.out.println("Scrambled Array");
    return myArray;
    
  }//end of Scramble method
  
  //create a linear search method
  
  public static void linear(int [] myArray){
    Scanner myScan = new Scanner(System.in);
    
    System.out.println("Enter a number to find through Linear Search ");
    int userLinear = myScan.nextInt();
    int linearTrack = 0;
    boolean found = false;
    
    for(int i = 0; i < 15; i++){
      linearTrack++;
     
      if (myArray[i] == userLinear){
        found = true;
        break;
      }//end of if statement
      
    }//end of for statement
    
    if(found = true){
      System.out.println("The Grade was found in " + linearTrack
                           + " iterations" );
    }//end of if statement
    else{
      System.out.println("The Grade was not found ");
    }//end of else statement
    
  }//end of linear method
  
  
  //Binary method
  public static void binary(int [] myArray){
    int low = 0;
    int high = myArray.length - 1;
  
   
    int iterations = 0;
    
    //create array within scope
    Scanner myScan = new Scanner(System.in);
    
    //ask user for grade to search for
    System.out.println("Enter a grade to be searched for (by binary search) ");

      int searchFor = myScan.nextInt();
      int binTrack = 1;
    
    
    while (low <= high){
      
      
  int    mid = (high + low) / 2;
      
      iterations++;//keep track of how many times I do it
      
      if (searchFor == myArray[mid]){
        System.out.println("Found it in " + iterations + " Iterations");;
        binTrack = 2;
        break;
      }//if it is the middle one
      
      else if(searchFor < myArray[mid]){
        high = mid - 1;
      }//end of bottom half search
      
      else if(searchFor > myArray[mid]){
        low = mid + 1;
      }//end of being greater than mid
      

      else{
        System.out.println("Error, you got to the else statement");
        break;
      }
        
    }//end of while method
    
    if(binTrack == 1){
    System.out.println("Your key doesn't exist within the array. " + iterations + " iterations were used");
    }
   
        
      
  }//end of binary method
  
  
  
}//end of class
      
    