//Lyle Chamebrlain
//lab 02
//9/6/18


//setting up code with class

public class Cyclometer {
  
  
  //create loop
  public static void main(String args[]){
    
    int secsTrip1=480;  // seconds for the first trip
    int secsTrip2=3220;  //seconds elapsed for the second trip
		int countsTrip1=1561;  //number of rotations for trip 1
		int countsTrip2=9037; //number of rotations for trip
    
    double wheelDiameter=27.0,  //constant wheel diameter
  	PI=3.14159, //mathematical symbol
  	feetPerMile=5280,  //constant conversion of mile and feet
  	inchesPerFoot=12,   //constant conversion of inches and feet
  	secondsPerMinute=60;  //constant conversion of seconds and minutes
	double distanceTrip1, distanceTrip2,totalDistance; //realting the two distances to the total distance

  System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) + " minutes and had "+ countsTrip1 + " counts."); //prints the total time and counts for trip one
  System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //Prints the same thing for trip 2
    
  //doingn calculations and storing values for trip 1
	distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1 /= inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //gives distance of trip 2
	totalDistance = distanceTrip1 + distanceTrip2; // gives the total distance by adding two trip distances
    
  
      System.out.println("Trip 1 was " + distanceTrip1 + " miles"); // prints out trip 1
	    System.out.println("Trip 2 was " + distanceTrip2 + " miles");//prints out trip 2
	    System.out.println("The total distance was " + totalDistance + " miles"); //prints the total distance

	



    
  
  
  
  
  
  //end of main method
  }
  
  
//end of class  
}