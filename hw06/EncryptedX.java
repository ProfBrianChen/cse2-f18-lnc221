//Lyle Chamberlain
//October 21 2018

//import scanner
import java.util.Scanner;

public  class EncryptedX{
  
  public static void main(String [] args){
    
  //create scanner
  Scanner myScan = new Scanner(System.in);

  //count will keep track of which column the code is in
  int count = 1;
  //rowTrack will keep track of which row the code is in
  int rowTrack = 1;
  //this keeps track of whether the user entered a valid number
  boolean correct = false;
  //this will give a random true flase value
  boolean random;
  
   //this will be immediately overwritten by user input
  int numRows = 0;
  
  
  //immediately go into while statement to check validity of statement
  while (correct == false){
    //ask for user input
    System.out.println("input number of rows");
    //check for int
    correct = myScan.hasNextInt();
    
        //if an int store it
        if (correct == true){
     
          //store user input as numRows
          numRows = myScan.nextInt();
  
         //now check numerical range
         if ((numRows < 0) || (numRows > 100)){
          // disregard break statement, and print error message for number not in range
          System.out.println("Error in input. Must be between 0 and 100");
   
          //make correct be false so that it will ask for user input again and will stay in loop
           correct = false;
           //don't drop out of while statement
             continue;
            }
    break;
    }
    System.out.println("Error in input. Must be an integer.");
  
    //clear scanner
    myScan.next();

   //end statement validating input 
  }
    
    //for loop for new lines
    for (int count2 = 1; count2 < numRows + 1; count2++){
      
      //rowTrack allows the next for loop to know when to print spaces
      rowTrack++;
      
      //print space
       System.out.println("");
           
           //for loop for each line
           for (count = 1; count < numRows + 1; count++){
     
          //create if statement for space
            //prints a space based off of what row and what collumn the code is in
                   if ((rowTrack - 1 == count) || (rowTrack == numRows - count + 2)){
        
                    //prints space that creates x
                   System.out.print(" ");
        
                   //if it prints a space we don't want it printing a star
                    continue;
                    }
      
            //print star if it doesn't go into if statement
          System.out.print("*");
          } 
    }
    
       //increment rowTrack
    rowTrack++; 
    
    
 //closing two brackets
  }
}