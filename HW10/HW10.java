//Lyle Chamberlain
//hw10

import java.util.Scanner;
import java.util.Arrays;
public class HW10{
  public static void main(String [] args){
    
    //create a scanner
    Scanner myScan = new Scanner(System.in);
    int drawCount = 0;
    int temp1 = 0;
int temp2 = 0;
    int [][] intBoard = new int[][]{{1,2,3},{4,5,6},{7,8,9}};
    String [][] stringBoard = new String[][]{{"1","2","3"},{"4","5","6"},{"7","8","9"}};

    //print initial array
     for(int i = 0; i < 3; i ++){
        System.out.println(Arrays.toString(stringBoard[i]));
    }//end of for loop
     
   boolean gameStatus = false;
   
   //used to decide x or o
    int whichLetter = -1;
    while(gameStatus == false){
    System.out.println(" What position would you like to mark? ");
    int userInput = valid(intBoard, stringBoard);//this validates the input(1-9) and whether it has been used before and spits out the userInput
    
    
    whichLetter *= -1;
      mark(intBoard, stringBoard,temp1, temp2, userInput, whichLetter);
      gameStatus = gameStatus(stringBoard);
    //print array
    for(int i = 0; i < 3; i ++){
        System.out.println(Arrays.toString(stringBoard[i]));
    }//end of for loop
      }//end of while loop
    
  }//end of main method


//create a method to check if entry is valid
public static int valid(int [][] intBoard, String[][] stringBoard){
  
   //recreate stringBoard
// String [][] stringBoard = new String[3][3];
int temp1 = 0;
int temp2 = 0;
int userInput = 0;
  
  
  Boolean testBoo = false;
    Scanner myScan = new Scanner(System.in);
  
  
  while(true){


    
testBoo = myScan.hasNextInt();

if(testBoo == false){
  System.out.println("Enter an integer");
  //testBoo =  myScan.hasNextInt();
  continue;
}

   userInput = myScan.nextInt();
 

if((userInput < 1) || (userInput > 9)){
  System.out.println("Enter an integer within range");
  myScan.nextLine();
  continue;
}



for(int i = 0; i < 3; i ++){
  for(int j = 0; j < 3; j ++){
    if(intBoard[i][j] == userInput){
     
      temp1 = i;
      temp2 = j;

    }
  }
}
      
      
   

 if((stringBoard[temp1][temp2] == "X")|| (stringBoard[temp1][temp2] == "O")){
   System.out.println("This position has already been played");
   continue;
 }


else{
  break;
}
  
    }//end of while loop
  

    return userInput;
  
}//end of valid method



  
//create method to replace variable 
public static String replaceString(int intMark) {
int replaceTemp = 0;
String myString = Integer.toString(intMark);
return myString;
}//end of replaceString method

//Method to place x's and o's
public static String [][] mark(int [][] intBoard, String [][] stringBoard, int temp1, int temp2, int userInput, int whichLetter ){

  String letter = "Error";
  
 //x or 0;
  if(whichLetter< 0){
    letter = "X";
  }
  else{
    letter = "O";
  }
  
  for(int i = 0; i < 3; i ++){
  for(int j = 0; j < 3; j ++){
    if(intBoard[i][j] == userInput){
     
     temp1 = i;
      temp2 = j;

    }
  }
  }   
  
 stringBoard[temp1][temp2] = letter;
  return stringBoard;

}

public static boolean gameStatus(String [][] stringBoard){
  int Xcounter = 0;//for horizontal
  int Ocounter = 0;
  int verticalX = 0;
  int verticalO = 0;
  

 
  
  boolean gameStatus = false;
  //check for rows
  for(int i = 0; i < 3; i ++){
    Xcounter = 0;
    Ocounter = 0;
    
    
    for(int j = 0; j < 3; j ++){
      if(stringBoard[i][j] == "X"){
        Xcounter++;
      }
      if(stringBoard[i][j] == "O"){
        Ocounter++;
      }
      
      if(Xcounter == 3){
      gameStatus = true;
      System.out.println("X's win! ");
      break;
      }
      
      if(Ocounter == 3){
        gameStatus = true;
        System.out.println("O's win! ");
        break;
      }
    }//end of inner for loop
  }//end of outer for loop
  
 
  //test for vertical
  for(int i = 0; i < 3; i ++){
    if(gameStatus == true){
      break;
    }
    verticalX = 0;
    verticalO = 0;
    for(int j = 0; j < 3; j ++){
      
      
      if(stringBoard[j][i] == "X"){
        verticalX ++;
      }
      if(stringBoard[j][i] == "O"){
        verticalO ++;
      }
      
  if(verticalX == 3){
      gameStatus = true;
      System.out.println("X's win! ");
      break;
      }
      
      if(verticalO ==3){
        gameStatus = true;
        System.out.println("O's win! ");
        break;
      }
  
    }
  }//end of vertical test
  
  if(((stringBoard[0][0] == "X") && (stringBoard[1][1] == "X") && (stringBoard[2][2] == "X")) || ((stringBoard[0][2] == "X") && (stringBoard[1][1] == "X") && (stringBoard [2][0] == "X"))){
    gameStatus = true;
    System.out.println("X's win! ");
  }
  if(((stringBoard[0][0] == "O") && (stringBoard[1][1] == "O") && (stringBoard[2][2] == "O")) || ((stringBoard[0][2] == "O") && (stringBoard[1][1] == "O") && (stringBoard [2][0] == "O"))){
    gameStatus = true;
    System.out.println("O's win!");
  }
      
  int drawCount = 0;
  for(int i = 0; i < 3; i ++){
    for(int j= 0; j < 3; j ++){
      if( (stringBoard[i][j] == "X") || (stringBoard[i][j] == "O")){
        drawCount++;
      }
    }
  }
  if((drawCount == 9) && (gameStatus == false)){
    gameStatus = true;
    System.out.println("It's a draw! ");
  }
  
  
return gameStatus;
}//end of gameStatus method

}//end of class
        
    
     