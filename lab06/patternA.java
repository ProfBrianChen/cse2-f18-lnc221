//Lyle Chamberlain
//lab06
//pattern A

//import scanner
import java.util.Scanner;

//start class and void loop
public class patternA{
  public static void main(String [] args){

    
    //set variable for user input
    int rowInput;
    
        //creat scanner to accept answer
    Scanner myScan = new Scanner(System.in);
    
    //ask user for how many rows
    System.out.print("How many rows would you like to print out? Input a number between 1 and 10 ");
    

    //only accept ints
   boolean hasInt = myScan.hasNextInt();
    
    if (hasInt == true){
      rowInput = myScan.nextInt();
    }
    else{
      rowInput = 0;
      myScan.next();
    }
    
    //accept user input
    while ((hasInt == false) || (rowInput > 10) || (rowInput < 1)){
       System.out.println("Error, please input an integer between 1 and 10");
    
   
            //test for int again
 hasInt = myScan.hasNextInt();
 
 if (hasInt == false){
 myScan.next();
 }
      
      if(hasInt == true){
    //if it gets to here 
    rowInput = myScan.nextInt();
      }
      
      else{
        //if not an int just clear it off
        rowInput = 0;
      }
    
    }

    //create for loop for how many rows to print
    for(int numRows = 1; numRows < rowInput + 1; numRows++){
      System.out.println("");
     /// System.out.println
        for(int count = 1; count < numRows + 1; count++){
          System.out.print(count + " ");
          
        }
      
    }
    
    //finish on new line
    System.out.println("");
    //ending two brackets
    
  }
}