import java.util.Random;
import java.util.Scanner;

public class lab07{
  public static void main(String [] args){
    
    //create a random number generator for 0-9
    Random myRand = new Random();
    
    //create a variable for the user input
    
    //create a scanner in case the user wants to do it again
    Scanner myScan = new Scanner(System.in);
    boolean booleanTest = true;
    do{
    
    //create all variable that will be used
   String adjective = adjectiveClass();
 
   String subjectNoun = subject();
  String verb = verb();
  String objectNoun = objectNoun();
  
  System.out.println("The " + adjective + " " + adjectiveClass() + " " + subjectNoun + " " + verb + " the " + objectNoun);
  System.out.println("Do you want to run the loop again? type 'true' for yes and 'false' for no");
  
 booleanTest = myScan.nextBoolean();
  
  
    } while(booleanTest == true);
  
   //ending bracket for main method
  }


//class to get adjective
public static String adjectiveClass(){

  int adjTest = randomize();
  String adjective = "Error in adjectiveClass";
  switch(adjTest){
    case 0:
   adjective = "hot";
   break;
   
    case 1:
   adjective = "tall" ;
     break;
   
    case 2:
   adjective = "weird";
   break;
   
    case 3:
   adjective = "grumpy";
     break;
   
    case 4:
   adjective = "small";
   break;
   
    case 5:
   adjective = "skinny";
   break;
   
    case 6:
   adjective = "athletic";
   break;
   
    case 7:
   adjective = "smart";
   break;
   
    case 8:
   adjective = "crazy";
   break;
   
    case 9:
   adjective = "sick";
   break;
   
    default:
   System.out.print("Error in adjective class");
   break;
   
  } //end of switch statment
   
   return adjective;
   
 }//end of adjective class
   

//Create a method for nounds appropriate for subject of sentence
public static String subject(){
   int subjectTest = randomize();
   String subjectNoun = "error in subject method"; 
   
   
   switch(subjectTest){
    case 0:
    subjectNoun = "giraffe";
   break;
   
    case 1:
subjectNoun = "otter";
     break;
   
    case 2:
subjectNoun = "hawk";
   break;
   
    case 3:
subjectNoun = "baby";
     break;
   
    case 4:
subjectNoun = "rabbit";
   break;
   
    case 5:
subjectNoun = "rat";
   break;
   
    case 6:
subjectNoun = "ant";
   break;
   
    case 7:
subjectNoun = "fish";
   break;
   
    case 8:
   subjectNoun = "cow";
   break;
   
    case 9:
subjectNoun = "hippo";
   break;
   
    default:
   System.out.print("Error in subject noun method");
   break;
   
   
   
   }//end of switch 
   return subjectNoun;
}//end of subject noun statement

//Create a method for nounds appropriate for past tense verbs
public static String verb(){
   int verbTest = randomize();
   String verb = "error in verb method"; 
   
   
   switch(verbTest){
    case 0:
 verb = "fought";
   break;
   
    case 1:
verb = "kissed";
     break;
   
    case 2:
verb = "passed";
   break;
   
    case 3:
verb = "punched";
     break;
   
    case 4:
verb = "hugged";
   break;
   
    case 5:
verb = "tripped";
   break;
   
    case 6:
verb = "watched";
   break;
   
    case 7:
verb = "robbed";
   break;
   
    case 8:
   verb = "smacked";
   break;
   
    case 9:
verb = "slapped";
   break;
   
    default:
   System.out.print("Error in verb method");
   break;
   
   
 
   
   }//end of switch 
     return verb;
}//end of subject verb statement

//Create a method for nounds appropriate for past tense verbs
public static String objectNoun(){
   int objectNounTest = randomize();
   String objectNoun = "error in objectNoun method"; 
   
   
   switch(objectNounTest){
    case 0:
 objectNoun = "Slug";
   break;
   
    case 1:
objectNoun = "lizard";
     break;
   
    case 2:
objectNoun = "salmon";
   break;
   
    case 3:
objectNoun = "moose";
     break;
   
    case 4:
objectNoun = "whale";
   break;
   
    case 5:
objectNoun = "monkey";
   break;
   
    case 6:
objectNoun = "orangutan";
   break;
   
    case 7:
objectNoun = "gorilla";
   break;
   
    case 8:
   objectNoun = "ladybug";
   break;
   
    case 9:
objectNoun = "snake";
   break;
   
    default:
   System.out.print("Error in object noun method");
   break;
   
   }//end of switch 
   return objectNoun;
}//end of object noun statement


   
  

  //Create a random class
  public static int randomize(){
    Random myRand = new Random();
    int randomNumber = myRand.nextInt(10);
    return randomNumber;
    

    
      //ending method for lab07
}
 


  
  
}//end of class
  
  