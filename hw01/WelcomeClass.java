//lnc221
//Lyle Chamberlain
//September 4, 2018
//hw 1

//setting up code
public class WelcomeClass{
  
  public static void main(String args[]){
  
//print out initial line
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    //same as first line printed
    System.out.println("  -----------");
    System.out.println(  "  ^  ^  ^  ^  ^  ^  ");
    //slashes count as something so use \\ instead of \
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    //customized letters, use lehigh id
    System.out.println("<-L--N--C--2--2--1->");
    //same thing, have to use \\ just to print /
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v ");
    
    
  }
}