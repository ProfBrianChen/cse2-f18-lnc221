//Lyle Chamberlain
//September 10, 2018
//hw02


//set up class as arithmetic
public class Arithmetic {
  
  //set up loop
  public static void main(String args[]){
    
    //initialize vaiables used for tax
    double pantsTax;
    double beltsTax;
    
    //initialize the total spent on each type of apparel
    double totalCostPants;
    double totalBeltCost;
    
    //initialize the total spent on both
    double totalCost;
    
    //initialize the total sales tax
    double totalTax;
    
    //initialize the total paid for entire transaction
    double totalPaid;
    
    //initialize the tax that will be translated to int
     // double pantsTaxDouble;
      //double beltsTaxDouble;
    
    //number of pairs of pants
    int numPants = 3;
    //cost per pair of pants
    double pantsPrice = 34.89;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //total spent for pants
    totalCostPants = numPants * pantsPrice;
    //Total spent for belts
    totalBeltCost = numBelts * beltCost;
    
    //sales tax for total pants purchase
     pantsTax = totalCostPants * paSalesTax;
     beltsTax = totalBeltCost * paSalesTax;
    
    //Translate tax into a point with only two decimal points
    //pantsTax = (pantsTaxDouble * 100) / 100.0;
    //beltsTax = (beltsTaxDouble * 100) / 100.0;
    
    //get total cost of both
    totalCost =  totalBeltCost + totalCostPants;
    
    //get total tax on both
    totalTax = pantsTax + beltsTax;
  
    
    //get total paid on transaction, costs + sales tax
    totalPaid = totalCost + totalTax;

    
    
    //all of this is the only way I could figure out how to get the two decimal points
    String numberAsString = String.format ("%.2f", totalTax);
    String second = String.format ("%.2f", pantsTax);
    String third = String.format ("%.2f", beltsTax);
    String fourth = String.format ("%.2f", totalPaid);
    
      


    
    //print total cost of pants
    System.out.println("Total cost of pants = " + totalCostPants + ". Tax paid on pants " + second);
   //print tax paid on that
    
    //print cost of belts plus tax on belts
     System.out.println("Total cost of Belts = " + totalBeltCost + ". Tax paid on belts " + third);
    
    //Print total purchases before tax
    System.out.println("Total cost of purchases before tax = " + totalCost);
    
    //print total sales tax
    System.out.println("Total sales tax = " + numberAsString);
    
    //print out total cost of purchases including sales tax
    System.out.println("Total cost of purchases with sales tax = " + fourth);

    
    
   
  }
  
  
  
  
}