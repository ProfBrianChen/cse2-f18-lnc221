//Lyle Chamberlain
//Lab 05

//import scanner
import java.util.Scanner;

//set class and loop
public class readingErrors{
public static void main(String [] args){
  
  //set booleans that needs to be true for program to continues
  boolean correct1 = false;
  boolean correct2 = false;
  boolean correct3 = false;
  boolean correct4 = false;
  boolean correct5 = false;
  boolean correct6 = false;
  
  //classify variables needed
  int courseNumber = 0;
  String departmentName = "";
  int numTimes = 0;
  String startTime = "";
   String instructorName = "";
  int numStudents = 0;
  
  //create Scanner
  Scanner myScan = new Scanner(System.in);
  
  //set while loop
  while (correct1 == false){
  //ask for course number
  System.out.println("Input the course number ");
    //use scanner to accept number
     
   correct1 = myScan.hasNextInt();
    //error message
   if (correct1 == true){
     courseNumber = myScan.nextInt();
     break;
   }
      System.out.println("Error in input, must be an integer");
    //clear Scanner 
    myScan.next();
  }

  
  //set loop for departement name
  while (correct2 == false){
    
    //ask for department name
    System.out.println("Input department name");
    //use scanner to accept it
    correct2 = myScan.hasNext();
    
    //creat break statement to avoid error message
    if (correct2 == true){
      departmentName = myScan.next();
      break;
    }
    //clear scanner
        myScan.next();
    System.out.println("Error in input, must be a string");
  }
  
  
  while (correct3 == false){
    
    //ask how many time class meets in a week
    System.out.println("How many times do you meet in a week?");
    //use scanner to accept data
    correct3 = myScan.hasNextInt();
    //store if true
    if (correct3 == true){
      numTimes = myScan.nextInt();
      break;
    }
    //Clear Scanner
    myScan.next();
    //error statement
    System.out.println("Error in input, must be an integer");
  }
  
  
  //start time loope
  while (correct4 == false){
    //ask what time class starts
    System.out.println("What time does class start?");
    //use scanner to accept answer and test validity
    correct4 = myScan.hasNext();
    //store if true
    if (correct4 == true){
      startTime = myScan.next();
      break;
    }
    //clear scanner and print error statement
    myScan.next();
    System.out.println("Error in input, must be a string");
  }
 
  //get instructor name
  while (correct5 == false){
    //ask for instructor name
    System.out.println("Input insructor name ");
    //use scanner to test input
    correct5 = myScan.hasNext();
    //store if ture
    if (correct5 == true){
      instructorName = myScan.next();
      break;
    }
    //clear scanner and print error
    myScan.next();
    System.out.println("Error in input, must be a string");
  }
  
  //get number of students
  while (correct6 == false){
    //ask for number of students
    System.out.println("Input number of students ");
    //use scanner to test input
    correct6 = myScan.hasNextInt();
    //store if true
    if (correct6 == true){
      numStudents = myScan.nextInt();
      break;
    }
    //clear scanner
    myScan.next();
    System.out.println("Error in input, must be an integer");
  }
  
  
//ending two closing curly brackets  
}  
}