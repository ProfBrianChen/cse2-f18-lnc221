//Lyle Chamberlain
//hw03
//ipmport scanner
import java.util.Scanner;

//set up class and loop
public class Convert{
    public static void main(String [] args){
      
      //set up myscanner for user input
      Scanner myScanner = new Scanner(System.in);
      
      //print statement for user input
      System.out.print("Enter the affected area in acres: ");  
      
      //get value from scanner
      double affectedAcres = myScanner.nextDouble();
      
      //new line
      System.out.print("");
      
      //ask user for rainfall 
      System.out.print("Enter the rainfall in the affected area: ");
      
      //new line
      System.out.print("");
      
      //accept user input as inches
      double rainfall = myScanner.nextDouble();
      
      //set equation for cubic miles
      //one acre = 5273000 square inches
      //cubic miles =  .00000000000000393147 cubic inches
     
      //get acres to square inches
     double affectedInches = affectedAcres * 6273000;
      
      //get cubic inches
      double cubicInches = affectedInches * rainfall;
      
      //cubic miles
      double cubicMiles = cubicInches *  .00000000000000393147;
      
     
      
      
      
      System.out.print(cubicMiles + " cubic miles");
 
      
      
      
    }
  
  
 
  
}