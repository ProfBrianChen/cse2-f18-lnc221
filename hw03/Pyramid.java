//Lyle Chamberlain
//pyramid

//get scanner
import java.util.Scanner;

//set up class and loop
public class Pyramid {
    public static void main(String [] args){
    
    Scanner myScanner = new Scanner(System.in);
    
    //ask for user input of square side of pyramid
    System.out.print("The square side of the pyramid is (input length): ");
    
    //get user input
    double squareSide = myScanner.nextDouble();
    
    //get a new line
    System.out.print("");
    
    //ask user for the height of the pyramid
    System.out.print("The height of the pyramid is (input height): ");
    
    //new line
    System.out.print("");
    //accept user input
    double height = myScanner.nextDouble();
    
    double volume = (squareSide * squareSide * height) / 3;
    
    //print out newly caluclated volume
    System.out.print("The volume inside the pyramid is: " + volume + ".");
    
    
    
    
    
    }
    }