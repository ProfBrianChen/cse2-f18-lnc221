//Lyle Chamberlain
//hw05

//import random generator
import java.util.Random;
//import scanner
import java.util.Scanner;

public class hw05{
  public static void main(String [] args){
    
    //start count for 'for' loops
    int count1 = 1;
    //create values for 5 card values in a hand
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    //create all variables that will be used throughout the code
      int pairTrack = 0;
      int threeTrack = 0;
      int fourTrack = 0;
      double totalPair = 0;
      double totalThree = 0;
      double totalFour = 0;
      double totalTwoPair = 0;
      int twoPairTracker = 0;
     
     //create values that will be used to access the array 
     int i = 0;
     int j = 1;

     //create counts that can be used to avoid endless for loops
     int count2 = 0;
     int count3 = 0;
     //create integer array to store card numbers and make them easier to access
    //must use the array statement from Zybooks to accomplish task
    int testArray[];
    //create scanner
    Scanner myScan = new Scanner(System.in);
    //create random generaotr
    Random randGen = new Random();
    //ask user for amount of loops
    System.out.println("Choose number of times to run the simulation ");
    //get user input and store it as number of loops
    int numLoops = myScan.nextInt();
    myScan.close();
    //to elimate error statemetn close scan
    //while statement that will run simulations with as many hands as specified
    while (count1 < (numLoops + 1)){
     
      //generate 5 random cards
      card1 = randGen.nextInt(51) + 1;
   
      card2 = randGen.nextInt(51) + 1;
      //guard against duplicate cards
      while (card2 == card1){
        card2 = randGen.nextInt(51) + 1;
      }
      card3 = randGen.nextInt(51) + 1;
      while ((card3 == card2) || (card3 ==card1)){
        card3 = randGen.nextInt(51) + 1;
      }
      card4 = randGen.nextInt(51) + 1;
      while((card4 == card3) || (card4 == card2) || (card4 == card1)){
        card4 = randGen.nextInt(51) + 1;
      }
      card5 = randGen.nextInt(51) + 1;
      while((card5 == card4) || (card5 == card3) || (card5 == card2) || (card5 == card1)){
        card5 = randGen.nextInt(51) + 1;
      }
            
      //create code to get card into a specific card value to easily compare
      while (card1 > 13){
        card1 = card1 - 13;
      }
      while (card2 > 13){
        card2 = card2 - 13;
      }
      while (card3 > 13){
        card3 = card3 - 13;
      }
      while (card4 > 13){
        card4 = card4 - 13;
      }
      while (card5 > 13){
        card5 = card5 - 13;
      }         
      //create array of new card numbers
      //use zybooks array lesson from assignment
      testArray = new int[]{card1,card2,card3,card4,card5};
      //SUBSTANTIAL SECTION: This makes sure each individual card is compared to all the other 4 cards. Runs 5 times for the 5 card hand
      while (count2 < 5){
        count3 = 0;
        
        //creat while loop to compare a singular individual card is compared to every other one, runs 4 times
        while (count3 < 4){
          
         //if theres a match, for the two cards enter loop
          if (testArray[i] == testArray[j]){    
            //might be more than just a pair, so if theres already two same value cards in the hand, this will recognize it is three of a kind, not a pair
            if (pairTrack > 0){
              pairTrack--;
            threeTrack++;
          }
            //if there are 4 same value cards, this shows that it is not three of a kind, but four of a kind
            else if(threeTrack > 0){
              fourTrack++;
                threeTrack--;
            }
          
          //else for the first if. If there is no previous values that match, the found matched pair will create a pair
          else {
            pairTrack++;
          }
          //if the cards don't equal, nothing happens and the card in 'i' is compared to the next card
          }

          //j is incremented so that the next number in the array is accessed
  j++;
  //don't let j go over 4 because value doesn't exist. go back to first card
  if (j > 4){
    j = 0;
  }
  //don't get trapped in loop
  count3++;
 
        }
        
      //info to reset loop for second card. You are going to the next card so you can compare it to its 4 other cards
count2++;
//compare next card
  i++;
  //make sure j doesn't equal i because it would return a pair that doesn't exist.
  j = i + 1;
    //don't let j go over 4
  if (j > 4){
    j = 0;
  }
  
  
  //  store final totals so that all 'Track' statements can be reset to keep track of differentiting pairs, three of a kind, and four of a kind
  totalPair = totalPair + pairTrack;
  totalThree = totalThree + threeTrack;
  totalFour = totalFour + fourTrack;

        //keep track of total amount of two pairs
twoPairTracker = twoPairTracker + pairTrack;
          
  //reset all cardTrackers so that there are not previous pairs in the system
  pairTrack = 0;
  threeTrack = 0;
  fourTrack = 0;
  
  }
      
      //add to the count that sets the while loop
      count1++;
    
      //set all values back to their origignal zero zalues because we changed their value for the previous 5 card hand
      i = 0;
      j = 1;
      count2 = 0;
      
      //my code double counts pairs. To make up for this, if there are 4 pairs in a singular 5 hand set (actually 2 pairs) the pairs are removed and a 2 pair is incremented
      if (twoPairTracker == 4){
      totalTwoPair++;
      pairTrack = pairTrack - 4;
       }
      
       //needs to be set to zero so next interation will only return two pair if there is indeed two pairs
        twoPairTracker = 0;
        
      //finishing bracket for how many times to run
    } 
    
      //because of the syntax of the code, it compares each value to every other card so my pair tracker is recording every pair twice, ever three, three times, and every 4 four times.
      //account for true amount of pairs etc
   
      totalPair = totalPair / 2;
     totalThree = totalThree / 3;
      totalFour = totalFour / 4;
    
      //get proportions
      double pairProp = (totalPair / numLoops);
      double threeProp = (totalThree / numLoops);
      double fourProp = (totalFour / numLoops);
      double twoPairProp = (totalTwoPair / numLoops);
      
//make a print statement for the proportions as the assignement says
      System.out.println("Number of Loop = " + numLoops);
      System.out.printf("Proportion of pairs = %1.3f%n", pairProp);
      System.out.printf("Proportion of Three of a Kind = %1.3f%n", threeProp);
      System.out.printf("Proportion of four of a kind = %1.3f%n", fourProp);
      System.out.printf("Proportion of Two Pairs = %1.3f%n", twoPairProp);
      
  //final two closing brackets    

}
}