//Lyle Chamberlain
//lab08

import java.util.Random;

public class lab08{
  public static void main(String [] args){
    
    //create two arrays
    int [] myArray = new int[100];
   // int [] otherOne = new int[100];
    //create a array to track occurences
    int [] howMany = new int[100];
    
    //create a random number generator
    Random myRand = new Random();
    
    //create for loop to random values in first array
    for (int i = 0; i < 100; i++){
     myArray[i] = myRand.nextInt(100); 
      
      //find out if theres a match
    howMany[myArray[i]] = howMany[myArray[myArray[i]]]+ 1;
    } //end of for loop
    
    for(int i = 0; i<100; i++){
      System.out.print(i + " occurs " + howMany[i]);
        
        //times vs time
        if (howMany[i] == 1){
        System.out.println(" time ");
      }//end of if statement
      else{
        System.out.println(" times ");
      }//end of else loop
      
    }//end of second for loop
    
    
  }
}