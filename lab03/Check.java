//Lyle Chamberlain
//lab03

//needs to be first step, to import scanner function
import java.util.Scanner;

//initialize public class
public class Check {
  // start main method 
  public static void main(String [] args) {
    
    //let scaner know you want to use it
    Scanner myScanner = new Scanner (System.in);
    
    //get original cost of check from user
    System.out.print("Enter the original costs of the check in the form xx.xx: ");
    
    //make the check cost a double
    double checkCost = myScanner.nextDouble();
    
    //ask user for the tip percentage that they want to pay
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ");
    
   //user inputs tip percentage and its stored as a double
    double tipPercent = myScanner.nextDouble();
    
    //make tip percentage a decimal
    tipPercent /= 100;
    
    //find out how many people went to dinner and have user input it
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //Print out the last output
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
          
   // calculate totalCost
    totalCost = checkCost * (1+ tipPercent);
    costPerPerson = totalCost / numPeople;
    
    //get a whole number for dollars
    dollars = (int)costPerPerson;
    
    //get dimers amount
    dimes = (int) (costPerPerson * 10) % 10;
    
    //get pennies amount
    pennies = (int)(costPerPerson * 100) % 10;
    
    //print final statement
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
  
    
    
    
    
    
    
    
    
    
  }
  
  
  
  
}