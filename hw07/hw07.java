//Lyle Chamberlain
//hw07

//import scanner
import java.util.Scanner;

//class
public class hw07{

//main method
public static void main(String [] args){
  
  //get user string input
String userString = userInput();

//counts to help with breaking out of loops
int a = 1;
int endIt = 0;


while (a == 1){  
  
switch(menu()){
  case 'c':
    System.out.println("Number of non-white space characters = " + nonWhite(userString));
    break;
    
  case 'w':
    System.out.println("number of words = " + getWords(userString));
   break;
   
  case 'f':
    System.out.println("instances = " + findText(userString));
    break;
    
  case 'r':
    System.out.println("Statement with replaced exlamation points = " + replacePoint(userString));
    userString = replacePoint(userString);
    break;
    
  case 's':
    System.out.println("Statement with shortened spaces = " + shortenSpace(userString));
    userString = shortenSpace(userString);
    break;
    
  case 'q':
    System.out.println("You have ended the program");
    endIt = 1;
    break;

                     
            default:
              System.out.print("Error in Switch Statement");
               break;
               

               

    

}



               if (endIt == 1){
                 break;
}


  
}//end of while loop



}//end of main method




public static String userInput(){
  System.out.println("Input a string of your choosing");
    
  //create scanner
  Scanner myScan = new Scanner(System.in);
  String fromMain = myScan.nextLine();
   System.out.println("You entered: " + fromMain);
  return fromMain;
}

//create menu method
public static char menu(){
 
  char theySay = 'z';
  //do while loop
  while ((theySay != 'c') && (theySay != 'w') && (theySay != 'f') && (theySay != 'r') &&(theySay != 's') && (theySay != 'q')){
  //set up scanner
  Scanner myScan = new Scanner(System.in);
  //print options
  System.out.println("\n\nMENU \n c - Number of non-whitespace characaters \n w - Number of words \n f - Find text \n r - Replace all !'s \n s - Shorten spaces \n q - Quit \n Choose an option:");

  //take in answer
    theySay = myScan.next().charAt(0);
    
    //don't print an error statement twice
    if((theySay != 'c') && (theySay != 'w') && (theySay != 'f') && (theySay != 'r') &&(theySay != 's') && (theySay != 'q')){
    }
    
    //break out of loop
    else{
      break;
    }
    
    // print error
    System.out.println("Enter a menu option");
  }  
  

  //check validity
 
  System.out.println("You selected option " + theySay);
  
  return(theySay);
  
 
}//end of menu method


//Create Number of non-White Spaces mthod
public static int nonWhite(String userString){
  
    //create scanner
  Scanner myScan = new Scanner(System.in);
  //get string length
  int stringLength = userString.length();
  
  //set a counter for the white spaces
  int whiteCount = 0;
  //test for white spaces
  for(int i = 0; i < stringLength; i++){
    if (userString.charAt(i) == ' '){
      whiteCount++;
    }//end of if loop
  }//end of for loop
  
  //substract white spaces from stringlength
  stringLength-=whiteCount;

  return stringLength;
  
}//end of nonWhite method


//Start of get number of words method
public static int getWords(String userString){

  //get string length
  int stringLength = userString.length();
  
  //create counts 
  int count1 = 0;
  int count2 = 0;
  
  
  //find words by finding letters with spaces before them
  for(int i = 1; i < stringLength -1; i++){
    if ((userString.charAt(i) != ' ') && (userString.charAt(i-1) == ' ')){
      count1++;
    }//end of if loop
    

    }//end of for loop
  
  //account for one letter words and first word
  if(userString.charAt(0) != ' '){
    count1++;
  }
  
  
    return count1;
  }//end of get words method
  

//create find text method
public static int findText(String userString){
 
    //create scanner
  Scanner myScan = new Scanner(System.in);
  
  //counter for matches
 int count = 0;
    //get string length so we know when to stop
  int stringLength = userString.length();
 
  System.out.println("What character combination are you looking for? ");
  String sampleText = myScan.nextLine();
  
    //get sample text length
  int sampleLength = sampleText.length();
  
//create for loop to review the text
  for(int i = 0; i < stringLength; i++){
    
      for(int j = 0; j < sampleLength; j++){
       
        if (sampleText.charAt(j) != userString.charAt(i + j)){
          break;
        }
        
        else{
           if(j + 1 == sampleLength){
          count++;
          break;
        }
          continue;
        }
      

  
                                        }//end of for loop
                                        }//end of outer for loop
  
  return count;
}//end of findText method


//Create replace explanation point methdo
public static String replacePoint( String userInput){
  
      //get string length so we know when to stop
  int stringLength = userInput.length();
  
  //make new string
  String myNewString = "";
  
  for(int i = 0; i < stringLength; i++){
   
   //create if statement for if its a !
    if(userInput.charAt(i) ==  '!'){
  myNewString = myNewString + '.';
  } //end of if statement
    else{
   myNewString = myNewString + userInput.charAt(i);
    }//end of else statement
    
    
}//end of for statement

  //return userInput as string
  return myNewString;
}//end of replacePoint method
  

//create shorten space class
public static String shortenSpace(String userInput){
  
  String myNewString = "";
    //get string length so we know when to stop
  int stringLength = userInput.length();
  
  //start a count
  int count = 0;
  
  for(int i = 0; i < stringLength;i++){
         
    if (userInput.charAt(i) != ' '){
        count = 0;
      }
    
    if (userInput.charAt(i) == ' '){
      count++;
      

      
      //if statement for more than one sace
      if (count > 1){
        myNewString = myNewString + "";
      }
      
      else{
        myNewString = myNewString + userInput.charAt(i);
      }
      

      
    }// end of outer statement
    
    else{
      myNewString = myNewString + userInput.charAt(i);
    }
    

      
    }//end of for loop statement
      //return statment
    return myNewString;
  } //end of shorten space method

  
}//end of class
  